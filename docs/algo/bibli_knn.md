# Transformer un code (kNN) en petite bibliothèque python

Pour pouvoir réutiliser le code qu'on a fait sur capytale pour l'algorithme kNN pour d'autres données que les pokémons, il est pratique d'en faire une _bibliothèque_, c'est à dire :

* des fonctions dans un fichier `knn.py` à part, bien écrites et documentées, qui ne font pas référence aux pokémons ou à un type de données en particulier.

* qu'on importe quand on en a besoin depuis un autre fichier python, par exemple avec `from knn import knn_classifie` qui fonctionnera si il y a bien une fonction `knn_classifie` dans un fichier `knn.py` accessible par python (si les fichiers sont dans le même dossier, c'est le cas).

## Étapes

* Créez un dossier `bibli_knn`, ouvrez thonny et enregistrez dans ce dossier des fichiers `knn.py`, `pokemons.py` et `bananes.py`;

* Copiez depuis capytale les fonctions utiles à kNN jusque `knn_classifie` dans `knn.py` en changeant les noms de variables qui font références aux pokémons. La fonction distance est un peu particulière : soit il faut ajouter des paramètres qui donnent les noms des clés sur lesquelles on calcule la distance, soit on peut choisir de ne pas la mettre dans la bibliothèque et de la faire refaire à chaque utilisation.

??? hint "plot2D"
    Si vous voulez utiliser plot2D, vous pouvez le remplacer par cette version :

    ```python
    def plot2D(data, cle_x, cle_y, cle_classe):
        """Affiche les points en 2D en fonction de cle_x,cle_y regroupés par valeurs de cle_classe"""
        from matplotlib import pyplot as plt
        from matplotlib.colors import TABLEAU_COLORS
        colors = list(TABLEAU_COLORS.values())
        plt.clf()
        plt.xlabel(cle_x)
        plt.ylabel(cle_y)
        
        classes = { p[cle_classe] for p in data }
        
        for i,typ in enumerate(classes):
            lx = [ float(p[cle_x]) for p in data if p[cle_classe] == typ]
            ly = [ float(p[cle_y]) for p in data if p[cle_classe] == typ]
            plt.scatter(lx,ly,label=typ,color=colors[i%len(colors)],s=1)
        plt.legend()
        plt.show()
    ```

* Copiez le reste du code dans le fichier `pokemons.py` et importez les fonctions nécessaires depuis `knn.py` pour avoir un code qui exécute kNN comme dans l'activité capytale. Téléchargez le fichier [pokemon.csv](pokemon.csv) dans ce même dossier et vérifiez que ça fonctionne.

* Faites de même dans le fichier `bananes.py` avec le fichier [bananes.csv](bananes.csv)

* Quelles parties du code sont répétitives entre `bananes.py` et `pokemons.py` ? Essayez de les intégrer sous forme de nouvelles fonctions dans `knn.py` pour que le code d'utilisation de la bibliothèque soit le plus simple possible dans les deux cas.