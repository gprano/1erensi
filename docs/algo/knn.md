# Algorithme kNN (k plus proches voisins)

L'algorithme knn est un algorithme :

* d'**apprentissage**, c'est à dire qu'il vise à apprendre des choses
  à partir de données.
* qu'on va utiliser ici pour des tâches de **classification** :
on cherche à mettre chaque donnée dans une catégorie.
* c'est un algorithme d'apprentissage **supervisé** : 
on va utiliser des données dont on connaît déjà la catégorie.

## Rappel sur la lecture de fichiers csv en python

Le format csv (comma separated values : valeurs séparées par des virgules)
permet de stocker des données structurées dans un fichier. C'est
moins puissant mais plus simple qu'une base de donnée.

Voici le début du fichier `iris.csv`:

```csv
sepal_length,sepal_width,petal_length,petal_width,species
5.2,2.7,3.9,1.4,versicolor
6.4,3.2,4.5,1.5,versicolor
5.0,3.5,1.6,0.6,setosa
4.7,3.2,1.3,0.2,setosa
5.0,2.3,3.3,1.0,versicolor
4.4,3.0,1.3,0.2,setosa
7.2,3.2,6.0,1.8,virginica
7.2,3.0,5.8,1.6,virginica
5.8,2.7,4.1,1.0,versicolor
```

En python, on peut utiliser le module csv :

```python
import csv

fichier = open("iris.csv")
donnees = list(csv.DictReader(fichier))
```

`donnees` contient alors une liste de dictionnaire où les clés sont les noms des colonnes, un par ligne :
```python
[ {'sepal_length': '5.2', 'sepal_width': '2.7', 'petal_length': '3.9', 'petal_width': '1.4', 'species': 'versicolor'}, 
  {'sepal_length': '6.4', 'sepal_width': '3.2', 'petal_length': '4.5', 'petal_width': '1.5', 'species': 'versicolor'},
  ... ]
```

On remarque que les valeurs sont de type `string`, il faudra les convertir en `float` si besoin.

## Programmation de kNN en python

[fichier zip contenant les fichiers du TP](TP_knn.zip)

Téléchargez et extrayez ce fichier.

Les données sont dans `iris.csv` et le fichier python à ouvrir et compléter dans Thonny est `knn_eleve.py`.


