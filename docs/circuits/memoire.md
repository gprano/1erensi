<script src="https://logic.modulo-info.ch/simulator/lib/bundle.js"></script>

# Ajouter de la mémoire dans un circuit logique

Avec la porte Nand, on a créé des circuits pour faire des calculs sur des nombres,
mais tous ces circuits font (presque) **instantanément** le calcul du résultat sur
leurs fils de sortie en fonction du courant électrique sur leurs fils d'entrée.

Si on change les valeurs en entrée, les valeurs en sortie changent automatiquement.
En conséquence, ces circuits ne pourront jamais avoir de mémoire.

## Le data flip-flop

On a besoin d'une nouvelle porte de base, le **data flip flop** (appelé aussi **bascule D**) qui fait quelque chose de très simple : envoyer en sortie ce qu'il y avait en entrée **au tour d'horloge précédent**.

Cette porte décale donc juste son entrée de 1 unité dans le temps.

Testez-là ci-dessous en changeant les valeurs de l'entrée :

<div style="width: 100%; height: 200px">
    <logic-editor mode="tryout">
        <script type="application/json">
            {
  "v": 4,
  "in": [
    {"type": "clock", "pos": [160, 90], "id": 6, "period": 2000, "name": "horloge"},
    {"pos": [160, 50], "id": 7, "name": "D (Données)", "val": 0}
  ],
  "out": [
    {"pos": [480, 50], "id": 8, "name": "Q (sortie normale)"}
  ],
  "components": [
    {"type": "flipflop-d", "pos": [330, 70], "in": [0, 1, 2, 3], "out": [4, 5], "state": 0}
  ],
  "wires": [[6, 0], [7, 3], [4, 8]]
}
        </script>
    </logic-editor>
</div>

## Créer un registre 1-bit avec un data flip flop

Un registre 1-bit permet de stocker un bit en mémoire.

Ce circuit a deux fils en entrée : 

* si "load" vaut 1, alors la valeur "entrée" sera stockée
en mémoire;
* si "load" vaut 0, la valeur précédente est conservée;

En sortie est affichée la valeur en mémoire.

Pour la fabriquer, il vous suffit de connecter la sortie du flipflop à son entrée, mais pas directement ! Il faut mettre une porte Mux qui va sélectionner entre:

* la sortie du flipflop (si load vaut 0)
* la nouvelle valeur en entrée à stocker (si load vaut 1)

Fabriquez et tester votre circuit ci-dessous (pour changer le nombre d'entrée et de sorties du mux, faites un clic droit dessus):

<div style="width: 100%; height: 300px">
  <logic-editor mode="design" showonly="mux">
    <script type="application/json">
{
  "v": 4,
  "in": [
    {"type": "clock", "pos": [160, 160], "id": 6, "period": 2000},
    {"pos": [160, 50], "id": 7, "name": "entrée", "val": 0},
    {"pos": [160, 100], "id": 9, "name": "load", "val": 0}
  ],
  "out": [
    {"pos": [640, 60], "id": 8, "name": "sortie"}
  ],
  "components": [
    {"type": "flipflop-d", "pos": [410, 80], "in": [0, 1, 2, 3], "out": [4, 5], "state": 0}
  ],
  "wires": [[6, 0]]
}
        </script>
  </logic-editor>
</div>

## Qu'est-ce que de la "RAM" ?

RAM veut dire _Random Access Memory_, c'est à dire de la mémoire à laquelle on peut accéder de manière "aléatoire", c'est à dire aux positions qu'on veut et pas
seulement dans l'ordre.

Cela signifie que les circuits de RAM auront toujours :
* une entrée _adresse_ qui va donner en binaire l'adresse du _mot mémoire_ qu'on veut
lire ou modifier
* une entrée _load_ pour savoir si on veut écrire ou seulement lire dans la mémoire
* une entrée _donnée_ avec la valeur qui sera écrite si _load_ vaut 1
* et enfin une sortie qui donne la valeur lue en mémoire

Voici pour exemple une RAM avec seulement deux registres à 1-bit, qui contient :

* le circuit de registre 1-bit dupliqué deux fois
* un _démultiplexeur_ qui permet d'envoyer la valeur de _load_ uniquement sur le registre
qui correspond à l'_adresse_
* un _multiplexeur_ en sortie qui permet de renvoyer la valeur lue du registre qui correspond à l'_adresse_

<div style="width: 100%; height: 400px">
    <logic-editor mode="tryout">
        <script type="application/json">
{
  "v": 4,
  "in": [
    {"type": "clock", "pos": [100, 340], "id": 6, "name": "horloge", "period": 2000},
    {"pos": [110, 30], "id": 7, "name": "donnée", "val": 0},
    {"pos": [100, 150], "id": 9, "name": "load", "val": 0},
    {"pos": [100, 270], "id": 39, "name": "adresse", "val": 1}
  ],
  "out": [
    {"pos": [690, 200], "id": 8, "name": "sortie"}
  ],
  "components": [
    {"type": "flipflop-d", "pos": [400, 140], "ref": "Registre0", "in": [0, 1, 2, 3], "out": [4, 5], "state": 1},
    {"type": "flipflop-d", "pos": [400, 310], "in": [25, 26, 27, 28], "out": [29, 30], "state": 0},
    {"type": "mux-2to1", "pos": [340, 240], "orient": "s", "in": [31, 32, 33], "out": 34},
    {"type": "demux-1to2", "pos": [240, 150], "in": [35, 36], "out": [37, 38]},
    {"type": "mux-2to1", "pos": [340, 70], "orient": "s", "in": [40, 41, 42], "out": 43},
    {"type": "mux-2to1", "pos": [560, 200], "in": [44, 45, 46], "out": 47}
  ],
  "wires": [[6, 0], [29, 31], [34, 28], [6, 25], [9, 35], [43, 3], [4, 40], [37, 42], [38, 33], [39, 36], [7, 41], [7, 32], [4, 44], [29, 45], [39, 46], [47, 8]]
}
        </script>
    </logic-editor>
</div>

Remarques :

* On a vu que dans les circuits de processeurs, les valeurs étaient stockées sur des blocs de plusieurs fils (4,8,16,32 ou 64 par exemple). On aura donc des registres qui stockent par exemple 4 bits, avec une entrée de donnée et une sortie aussi sur 4 bits. Cela permet de lire 4 bits de la mémoire d'un coup.
* S'il y a plus de 2 registres dans la RAM (et on espère !), l'adresse sera aussi sur plusieurs bits. Par exemple avec 3 fils pour l'adresse on peut avoir 8 adresses différentes qui vont de 000 à 111.

## Un exemple de RAM

Voici un exemple de RAM 16 registres de 4 bits, donc 16x4 bits représentés au milieu en noir pour 0 et jaune pour 1.

<div style="width: 100%; height: 400px">
    <logic-editor mode="tryout">
        <script type="application/json">
{
  "v": 4,
  "in": [
    {"pos": [180, 350], "orient": "n", "id": 63, "name": "load", "val": 0},
    {"type": "nibble", "pos": [100, 190], "id": [87, 88, 89, 90], "val": [1, 1, 1, 1], "name": "données"},
    {"type": "nibble", "pos": [240, 50], "orient": "s", "id": [91, 92, 93, 94], "val": [1, 1, 1, 1], "name": "adresse"},
    {"type": "clock", "pos": [100, 280], "id": 99, "period": 2000}
  ],
  "components": [
    {"type": "ram-16x4", "pos": [240, 190], "in": [100, 101, 102, 103, 104, 105, 106, 107, 108, 109, 110], "out": [111, 112, 113, 114]}
  ],
  "wires": [[91, 107], [92, 108], [93, 109], [94, 110], [87, 103], [88, 104], [89, 105], [90, 106], [99, 100], [63, 101]]
}
        </script>
    </logic-editor>
</div>

Essayez de reproduire le dessin suivant en manipulant le circuit :

![](circuit_obj.png)

Quelle est la liste en base 10 des 16 nombres ainsi stockés en mémoire ?