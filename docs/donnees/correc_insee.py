import csv

fichier = open("salaires_2017_20k.csv")
reader = csv.DictReader(fichier,delimiter=";")

salairesF = [0] * 24
salairesH = [0] * 24
for ligne in reader:
    tranche_salaire = int(ligne["TRNNETO"])
    if ligne["SEXE"]=="2":
        salairesF[tranche_salaire]+=1
    else:
        salairesH[tranche_salaire]+=1

import matplotlib.pyplot as plt
import numpy as np

def hbar_plot(l1, label1, l2, label2):
    """Affiche les données de l1 et l2 en barres"""
    n = len(l1)
    x = np.arange(n)
    plt.barh(x - 0.2,l2,0.4,color="gray",label=label2)
    plt.barh(x + 0.2,l1,0.4,color="purple",label=label1)
    plt.xlabel("nombre de personnes")
    plt.ylabel("catégorie de salaires")
    plt.legend()
    plt.show()


hbar_plot(salairesF,"Femmes", salairesH, "Hommes")