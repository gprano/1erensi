import csv

pays = list(csv.DictReader(open("pays.csv"),delimiter=";"))
villes = list(csv.DictReader(open("villes.csv"),delimiter=";"))

# Q1
min_lat = 100
for v in villes:
    if v["Country_ISO"] == "IN":
        if float(v["Latitude"]) < min_lat:
            min_lat = float(v["Latitude"])
            ville_indienne_sud = v["Name"]
print("La ville indienne la plus au sud est", ville_indienne_sud, "latitude",min_lat)

# Q2
for v in villes:
    if v["Name"] == "Grenoble":
        id_grenoble = v["Id"]
        pop_grenoble = int(v["Population"])
min_diff = 10**10
for v in villes:
    diff = abs(pop_grenoble - int(v["Population"]))
    if v["Name"]!="Grenoble" and diff < min_diff:
        min_diff = diff
        ville_pop_quasi_grenoble = v["Name"]
print("La ville avec la population la plus proche de Grenoble est",ville_pop_quasi_grenoble, "difference",min_diff)

# Q3
pays_iso_vers_continent = dict()
liste_continents = []
for p in pays:
    pays_iso_vers_continent[p["ISO"]] = p["Continent"]
    if p["Continent"] not in liste_continents:
        liste_continents.append(p["Continent"])
min_diff_continent = dict() # continent -> min_diff avec grenoble
min_ville_continent = dict()
for v in villes:
    diff = abs(pop_grenoble - int(v["Population"]))
    if v["Country_ISO"] not in pays_iso_vers_continent:
        continue
    continent = pays_iso_vers_continent[v["Country_ISO"]]
    if v["Name"] != "Grenoble" and (continent not in min_diff_continent or diff < min_diff_continent[continent]):
        min_diff_continent[continent] = diff
        min_ville_continent[continent] = v["Name"]
for continent in min_ville_continent:
    print("Dans le continent",continent,"la ville avec la pop proche de Grenoble est",min_ville_continent[continent],"diff",min_diff_continent[continent])

# Q4
from math import radians,cos,sin,asin,sqrt
def distance(ville1, ville2):
    lat1 = radians(float(ville1["Latitude"]))
    lon1 = radians(float(ville1["Longitude"]))
    lat2 = radians(float(ville2["Latitude"]))
    lon2 = radians(float(ville2["Longitude"]))
    
    difflat = abs(lat2 - lat1)
    difflon = abs(lon2 - lon1)
    hav = sin(difflat/2)**2 + cos(lat1) * cos(lat2) * sin(difflon/2)**2
    rayon = 6371
    dist = 2 * rayon * asin(sqrt(hav))
    return dist

for v in villes:
    if v["Name"] == "Grenoble":
        lat_gre = float(v["Latitude"])
        lon_gre = float(v["Longitude"])
        lat_opposee = -lat_gre
        lon_opposee = 180 - lon_gre
        fake_ville = {"Latitude" : lat_opposee, "Longitude" : lon_opposee}
min_dist = 10**10
for v in villes:
    if distance(fake_ville, v) < min_dist:
        min_dist = distance(fake_ville, v)
        nom_ville = v["Name"]
print("La ville la plus proche du point opposé de Grenoble est", nom_ville, round(min_dist),"km")

# arf il faut genre un KD-tree
# ou juste stocker les villes par lat/long et approcher
ville_from_id = dict()
for v in villes:
    ville_from_id[v["Id"]] = v
ville_par_latlon = dict()
for v in villes:
    id_ville = v["Id"]
    coo_arrondies = round(float(v["Latitude"])/2), round(float(v["Longitude"])/2)
    if coo_arrondies not in ville_par_latlon:
        ville_par_latlon[coo_arrondies] = []
    ville_par_latlon[coo_arrondies].append(id_ville)

def graphe_max_dist(max_dist):
    g = {v["Id"] : [] for v in villes}
    for v in villes:
        lat_int = round(float(v["Latitude"])/2)
        lon_int = round(float(v["Longitude"])/2)
        candidats_proches = []
        for lat in range(lat_int-1, lat_int+2):
            for lon in range(lon_int-1,lon_int+2):
                if (lat,lon) in ville_par_latlon:
                    candidats_proches.extend(ville_par_latlon[(lat,lon)])
        for v2_id in candidats_proches:
            if v["Id"] != v2_id and distance(v,ville_from_id[v2_id]) <= max_dist:
                g[v["Id"]].append(v2_id)
                #print(v["Name"],ville_from_id[v2_id]["Name"],distance(v,ville_from_id[v2_id]))
    return g

graphe = graphe_max_dist(180)

cible = "Shanghai"
most_east = 0
from collections import deque
file = deque()
file.append((id_grenoble,0))
vu = { v["Id"] : False for v in villes }
prev = dict()
while len(file)>0:
    identifiant,dist = file.popleft()
    vu[identifiant] = True
    if float(ville_from_id[identifiant]["Longitude"]) > most_east:
        print(ville_from_id[identifiant]["Name"],float(ville_from_id[identifiant]["Longitude"]))
        most_east = float(ville_from_id[identifiant]["Longitude"])
        cible_id = identifiant
    if ville_from_id[identifiant]["Name"] == cible:
        print("oui!")
        cible_id = identifiant
        break
    for voisin in graphe[identifiant]:
        if not vu[voisin]:
            vu[voisin] = True
            file.append((voisin, dist+1))
            prev[voisin] = identifiant
chemin = []
while cible_id in prev:
    chemin.append(cible_id)
    cible_id = prev[cible_id]
chemin.reverse()
for v in chemin:
    print(ville_from_id[v]["Name"],end=",")
print()