# Exposé sur une commande linux

Objectif : préparer un exposé d'environ 2min devant la classe pour expliquer et démontrer l'utilisation d'une commande linux.

* À l'oral;
* Avec démonstration sur la ligne de commande de l'ordinateur au vidéoprojecteur (sans autre support);

Pour tester les commande et préparer votre démo, si votre ordinateur n'utilise pas linux (ou iOS qui a une ligne de commande similaire !), vous pouvez :

* Utiliser le [sous-sytème windows pour linux](https://learn.microsoft.com/fr-fr/windows/wsl/install) qui permet d'installer une version de linux à l'intérieur de windows. C'est un peu plus compliqué à installer mais ça vous permet d'utiliser réellement linux ensuite.
* Ou bien, utiliser une version de linux en javascript qui peut tourner dans le navigateur web par exemple à [cette adresse](https://bellard.org/jslinux/vm.html?url=alpine-x86.cfg&mem=192) ou [sur le site onworks](https://www.onworks.net/os-distributions/ubuntu-based/free-ubuntu-online-version-22).

Liste des commandes (si des options sont précisées, il faut les montrer et les expliquer) :

* wget
* tree
* ls -l
* du (+ options -h et -s)
* htop
* groups
* ssh
* df (+ option -h)
* head (+ option -n)
* tail (+ option -n)
* traceroute
* which
* locate
* sort (+ option -n)
* zip et unzip
* diff
* host
* env
* apt search (et expliquer apt install mais on aurait besoin de sudo)
* strace (difficile, aide sous forme de zine en anglais ici https://jvns.ca/strace-zine-v3.pdf)
* wc (+options -m, -l, -w)
* shred (+option -u)
* ln -s
* last
* mount