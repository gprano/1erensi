# Derniers ajouts 2023/2024

* [Introduction](web/javascript.md) au langage javascript et [mini-projet](web/projets_js.md).

* [Fabrication de bibliothèque](algo/bibli_knn.md) pour l'algorithme kNN

* [Exposé](expose_commande.md) à préparer sur une commande linux

* [Créer une page web dynamique](os/tp_flask.md) avec un serveur en python

* [TP](web/reseau_web.md) de rappels de HTML/CSS.

* [TP](http://frederic-junier.org/NSI/premiere/chapitre25/tp/TP-Filius-NSI-2020V1.pdf) de simulation de réseau avec le logiciel Filius (fichiers pour l'[exercice 4](os/exercice4_ressources.fls) et l'[exercice 5](os/exo5.fls))

* Exercices de programmation sur [codex](https://codex.forge.apps.education.fr/) : Faire d'abord [années bissextiles](https://codex.forge.apps.education.fr/exercices/bissextile/), [les durées (II)](https://codex.forge.apps.education.fr/exercices/manipulation_des_durees-II/), [indice ou valeur](https://codex.forge.apps.education.fr/exercices/indices_valeurs/), [recadrer](https://codex.forge.apps.education.fr/exercices/recadrer/), [deux meilleurs](https://codex.forge.apps.education.fr/exercices/deux_meilleurs/), [aplatir](https://codex.forge.apps.education.fr/exercices/aplatir/), [addition binaire](https://codex.forge.apps.education.fr/exercices/addition_binaire/) et choisissez ensuite ceux que vous voulez.

* [Données au format csv](donnees/tp_csv.md) en python avec des dictionnaires, et [application](donnees/tp_insee.md) sur des données de l'INSEE.

* [Premier TP](os/tp1.md) de ligne de commande / réseau. [Second TP](os/tp3.md) et [extension](os/tp4.md).

* [Programmation en assembleur](circuits/assembleur.md) sur simulateur

* [Ajouter de la mémoire](circuits/memoire.md) aux circuits logiques

* [Deuxième projet](projets/projet2D.md) de jeu au tour par tour dans une grille 2D en mode texte

* [Lien](https://romainjanvier.forge.aeif.fr/nsipremiere/5_architecture_OS/1_circuits_logiques/1_nand/) vers le site de R.Janvier pour les circuits logiques

* [Premier projet](projets/rue.md) (dessin de rue avec le module turtle, création de fonctions)