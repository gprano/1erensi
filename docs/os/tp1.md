# Utilisation de la ligne de commande

<!--

## GameShell

Pour télécharger et lancer le jeu en ligne de commande Gameshell :

```bash
mkdir Gameshell
cd Gameshell
wget https://github.com/phyver/GameShell/releases/download/latest/gameshell.sh
bash gameshell.sh
```

Et si vous aviez déjà commencé le jeu, retourner dans le dossier et relancez-le (la progression est sauvegardée):

```bash
cd Gameshell
bash gameshell.sh
```

-->

## Un peu de réseau

### ping

Pour savoir si vous arrivez à contacter une machine, la commande **ping** envoie des messages et attend de recevoir des accusés de réception. Essayez :

```bash
ping ADRESSE
```

!!! info 
	ADRESSE doit être une adresse IP ou un nom de domaine. Vous pouvez essayer une IPv4 comme 149.91.88.90 ou un nom de domaine comme wikipedia.org, mais aussi le nom de la
	machine de votre voisin sur le réseau du lycée e202-...
	
	La commande ne s'arrête pas toute seule, il faut utiliser **Contrôle-C**, qui force l'arrêt du programme en cours dans le terminal.

Quelles informations obtenez-vous avec les réponses ?

Essayez d'en deviner le plus possible.

### wget

D'abord, créez un nouveau dossier pour ne pas éparpiller les fichiers qu'on va utiliser partout, et déplacez-vous dedans :

```bash
mkdir tp1_os
cd tp1_os
```

Puis utilisez **wget**, qui télécharge un fichier depuis internet dans le dossier où vous êtes :

```bash
wget nodfs.xyz/programmes.zip
```

Vérifiez avec `ls` que le fichier a été téléchargé. C'est une archive zip, on peut l'extraire avec :

```bash
unzip programmes.zip
```

Vous pouvez refaire `ls` puis utiliser `cat` si vous voulez voir la liste des fichiers obtenus et leur contenu.

## Exécuter un programme python

!!! info "Rappel"
	Python est un langage **interprété**, ça veut dire que pour exécuter un programme, on a toujours besoin du logiciel python qui va lire et exécuter le code ligne par ligne. Pour exécuter le fichier `programme.py` par exemple, on va entrer :

```sh
python3 programme.py
```

On utilise `python3` parce que sur certains systèmes `python` appelle la version 2 de python qui utilise un langage un peu différent et qui n'est plus beaucoup utilisé.

!!! info "Options de commandes et version de python"
	La plupart des commandes peuvent avoir des options qu'on ajoute sur la ligne de commande de cette façon:

	* `ping -6` utilise IPv6
	* `ping -t 10` met le TTL (time to live, nombre de rebonds sur des routeurs avant que le paquet soit supprimé) des paquets envoyés à 10. Cette option prenait un paramètre, qu'on a ici choisi à 10.
	
	Parfois les options ont des noms plus longs et commencent par un double tiret, c'est le cas de `--version` disponible pour beaucoup de commandes. Cette option permet souvent d'afficher la version et de quitter.

	Quelle est la version exacte de `python3` sur votre machine ? Et celle de `python` ?

Lisez le fichier `programme.py` avec `cat`, et exécuter-le. Que fait ce programme ?

??? hint "Réponse"
	Il affiche pour i allant de 0 à 14 la valeur de i et de 10 puissance i.

## Exécuter un programme en C

Lisez le fichier programme.c, qui contient un code qui fait la même chose mais dans le langage de programmation C. Qu'est-ce qui est différent entre les deux codes ? Qu'est-ce qui est commun ?

??? hint "Réponse"
	Parmi les différences entre C et python :
	
	* Le type des variables doit être indiqué explicitement : `int x = 0;`.
	* Les blocs sont marqués par des accolades {} au lieu de l'indentation (décalage).
   

Ce langage n'est pas interprété mais **compilé**, il faut donc deux étapes pour l'exécuter :

1. On utilise le compilateur gcc pour produire un fichier exécutable à partir du code. L'option `-o` permet de choisir le nom du fichier exécutable :
	```bash
	gcc programme.c -o programme
	```
2. On exécute le fichier exécutable qui a été créé. Le `./` au début permet d'exécuter un programme qui est dans le dossier actuel, car `.` est un raccourci pour le dossier actuel. Si on écrivait juste `programme`, le terminal chercherait cet exécutable dans les endroits où sont les logiciels installés sur l'ordinateur, et ne le trouverait pas.
	```bash
	./programme
	```

Obtient-on le même résultat qu'avec le programme python ?
  
??? hint "Réponse et Explication"
	Non, à partir de 10^10 les valeurs des puissances de 10 sont fausses.
	
	!!! info
		En fait, en python on a de la chance car les entiers peuvent être aussi grands qu'on veut. Dans la plupart des langages comme C, les entiers sont stockés avec un nombre de bits fixé (32 ou 64 en général). Comme 10^10 est plus grande que 2^32, on a un dépassement et le résultat est faux.
	
	!!! bug ""
		Dans un langage comme python, on s'attendrait à avoir une erreur pour nous informer. En C, il n'y a pas vraiment de vérification pendant l'exécution (il y en a certaines à la compilation), et donc certains bugs peuvent passer inaperçus. Ce type de bug (_integer overflow_ en anglais) est courant. En 1996, une fusée Ariane 5 s'est écrasée à cause d'un dépassement de la taille des entiers !
	
	Cette absence de vérification permet néanmoins de gagner du temps (C est un langage plus rapide que python).
  
Vous pouvez aussi regarder à quoi ressemble le fichier exécutable avec `cat`.

??? hint "Réponse"
	C'est illisible, parce que ce n'est pas un fichier avec du texte mais des instructions pour la machine directement en binaire !

	Vous pouvez affichez les instructions machine (en "assembleur") avec la commande `objdump -d FICHIER`

!!! info "Le saviez-vous ?"
	Le programme python lui-même, c'est à dire l'interpréteur qui permet d'exécuter du code python, est écrit... en C !
	
	S'il était écrit en python, on aurait un problème : il aurait besoin de l'interpréteur python pour s'exécuter, c'est à dire de lui-même, qui aurait donc encore besoin de lui-même... on tourne en rond.
	


## Mesurer la vitesse d'exécution

Que fait le code python contenu dans le fichier `compteur.py` ?

??? hint "Réponse"
	Il augmente de 1 la valeur d'une variable un million de fois, puis l'affiche.

Lisez le fichier `compteur.c`, qui fait la même chose en C.

Pour mesurer le temps d'exécution d'une commande, on peut utiliser :

```bash
time COMMANDE
```

Mesurez le temps d'exécution du programme python, puis compilez et mesurer le temps
d'exécution du programme C. Combien de fois plus rapide est la version en C ?

??? hint "Réponse"
	```bash
	time python3 compteur.py
	gcc compteur.c -o compteur
	time ./compteur
	```
	Ça peut varier selon les systèmes, mais de l'ordre de 20x plus rapide en C pour ce programme.

