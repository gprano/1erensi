# TP2 : adresses IP, DNS, protocoles

Lancez un terminal avec le raccourci Ctrl-Alt-T.

Installez les programmes nécessaires au TP avec les commandes :

```bash
sudo apt update
```

Puis

```bash
sudo apt install net-tools
```

!!! info
	`apt` est le _gestionnaire de paquets_ de la version de Linux qu'on utilise (qui s'appelle _Debian_).
	`apt update` permet de mettre à jour la liste des paquets disponibles, et `apt install` d'en installer un.
	Cette opération nécessite les droits administrateurs, c'est pour ça qu'on utilise `sudo` devant la commande
	(en général on aurait ensuite besoin d'un mot de passe, il n'est pas configuré sur ces systèmes sur clé usb).

1) Utilisez la commande `sudo ifconfig` pour trouver :

* votre adresse IP sur le réseau. Est-ce que vous avez aussi une IPv6 ?
* l'adresse MAC de la carte réseau

2) Cherchez "what is my ip" sur google pour trouvez l'adresse IP que les sites voient quand vous leur envoyez une requête.

* a) Est-ce la même ? Pourquoi ?
* b) Que peuvent savoir les sites internet sur vous avec cette adresse IP ?

3)

* a) Utilisez la commande `host` pour trouver l'adresse IP de `wikipedia.org`. 

!!! info
	Cette commande effectue une requête DNS, pour trouver l'adresse IP associée à un nom de domaine.
	Sur Linux, le ou les serveurs DNS utilisés sont définis dans le fichier `/etc/resolv.conf`.
	
* b) Utilisez la commande `host` pour faire une requête inversée, en lui donnant votre adresse IP que vous avez trouvée à la question 2.
  Quelle information obtient-on ?

4) Quels sont les serveurs DNS configurés sur votre machine ?

Allez à l'adresse indiquée au tableau avec le navigateur, et récupérez votre site internet sous forme zippée.

Dans le terminal, déplacez-vous là où est le fichier compressé et décompressez-le avec la commande `7z x NOMDUFICHIER`.

Déplacez vous dans le répertoire du site. On va lancer un petit serveur web qui va distribuer le contenu du répertoire où on est
avec la commande `python3 -m http.server PORT` en choisissant un PORT libre (nombre entier plus grand que 1024, par exemple 8080).

Demandez à la personne à côté de vous d'essayer d'accéder à votre site en tapant dans la barre d'adresse du navigateur
http://IP:PORT avec l'IP locale que vous avez trouvé, et le port que vous avez choisi.

5) Observez les requêtes qui sont affichées à la suite de la commande `python3 -m http.server`. 

* a) Que pouvez vous savoir sur la personne qui envoie les requêtes ?
* b) Quel est le nom de la méthode HTTP utilisée pour demander une ressource ? Et quels sont les codes de retour que vous voyez ?
* c) Est-ce que votre site est maintenant accessible depuis tout Internet ?
