# Projet dans une grille en 2D

Le but est de faire un petit jeu avec un affichage en mode texte, qui se passe dans une grille en deux dimensions.

## Déroulement du projet

* Sur 3 séances, à terminer pendant les vacances.
* Par groupe de 2 ou 3 élèves.
* Un lien nextcloud vous sera envoyé par groupe pour partager votre code et pouvoir avancer chez vous.
* Il faudra à la fin rendre : 
    * le code 
    * un fichier `readme.txt` qui explique votre progression, le partage du travail dans le groupe, ce que vous n'avez pas réussi à faire et pourquoi, les liens de l'aide éventuelle que vous avez utilisé. 
    * Il y aura une petite présentation orale à la classe à la rentrée.

## Première séance

Partez de ce que vous avez fait dans [l'activité capytale](https://capytale2.ac-paris.fr/web/c/27ee-1006711). Créez un dossier pour le projet, travaillez avec Thonny.

Rajoutez ensuite ce qu'il faut pour avoir un code qui :

* lit une grille depuis un fichier texte (remplie de cases vides avec `_` et de murs par exemple avec `X`) et place un personnage quelque part.
* faites une boucle `while True:` (donc infinie) qui à chaque tour :
    * affiche la grille avec la fonction faite
    * demande une action (avec `input()`)
    * si cette action est `z`, `q`, `s`, `d` déplacez le personnage _si c'est possible_, si l'action est `quit` terminer la boucle while avec l'instruction python `break`

??? info "Recommendations et conseils"

    * Définissez toutes les variables qui représentent l'état du jeu au même endroit, au début du code (la grille, les coordonnées du personnage, etc).
    * Choisissez des noms de variables compréhensibles.
    * Vous pouvez définir des variables qui ne changeront pas dans le jeu (des constantes) comme la LARGEUR et la HAUTEUR de la grille qu'on met par convention en majuscule.
    * Il est bien de définir des fonctions dans votre code pour principalement deux raisons :
        * ne pas surcharger la boucle while du jeu qui deviendrait illisible, par exemple au lieu de mettre les if/else pour chaque action, on peut définir avant dans le code une fonction `accomplir_action(action)` et l'appeler.
        * Dès que vous avez besoin de faire quelque chose plusieurs fois. Par exemple il sera utile d'avoir une fonction `est_valide(i, j)` qui renvoie `True/False` selon que la case de coordonnées (i,j) est bien dans la grille et n'est pas un mur.
    * Normalement dans python on ne peut pas changer dans une fonction une variable qui a été définie en dehors. Si vous avez vraiment besoin de le faire, il faut déclarer cette variable comme _globale_ au début de la fonction, par exemple :
    ```python
    def bouger(direction):
        global perso_i, perso_j
        # ...
    ```

## Personnalisation

Vous pouvez ensuite décider de ce que vous voulez faire comme type de jeu, quelques idées par exemple :

* en rajoutant des monstres, des objets à ramasser et des pièges dans la grille on peut faire un jeu d'exploration.

* on peut faire un jeu de labyrinthe où le but est d'arriver sur une certaine case objectif.

Beaucoup de variations sont possibles : 

* faire poser des bombes qui effacent les 8 cases autour
* avoir des caisses qu'on peut déplacer en les poussant
* un inventaire avec des clés à ramasser qui débloquent certaines portes
* un brouillard qui fait que seule une certaine zone autour du personnage est visible
* des éléments qui se déplacent au hasard à chaque tour
* des cases pour se téléporter
* deux personnages qui se déplacent de la même façon

Les limites sont qu'il sera plus facile de garder le jeu au tour par tour (pas de rapidité, même si un chrono est envisageable) et avec un caractère par case.

L'originalité et l'inventivité sont encouragés: il faudra à chaque fois trouver la manière de faire en python ce qui aura l'effet voulu. Il y a souvent plusieurs solutions possibles.

!!! warning "Important"
    Mais il faut partir sur une idée la plus simple possible à faire, et ajouter les fonctionnalités au fur et à mesure.

    Un piège presque inévitable dans les projets de programmation est de sous-estimer grossièrement la difficulté et le temps que ça prendra. Tenez en compte !

Il sera aussi possible d'ajouter de la couleur à l'affichage, voire de rendre le jeu plus rapide en détectant les touches du clavier sans devoir appuyer sur Entrée (aide ci-dessous).

!!! hint "Récupérer directement les appuis de touche du clavier"
    Pour récupérer les touches sans avoir besoin d'appuyer sur Entrée à chaque fois, on peut utiliser le module `get-key`.

    Dans un terminal linux, tapez (pip est le logiciel gestionnaire des modules python) :

    ```bash
    pip3 install get-key
    ```

    Puis dans le code il suffit de faire :
    ```python
    from getkey import getkey

    touche = getkey()
    if touche == 'a':
        # ...
    ```

    **Attention** dans Thonny cela ne marchera pas dans la console Thonny, il faudra utiliser le menu "Exécuter/Exécuter le script courant dans un terminal"

    Les flèches renvoient des codes spéciaux, si vous voulez les utiliser on peut comparer ces codes avec des valeurs du module :
    ```python
    from getkey import getkey, keys

    if touche == keys.UP: # flèche du haut
        #...
    # idem keys.DOWN, keys.LEFT, keys.RIGHT
    ```

    La liste complète des touches est [là](https://github.com/kcsaff/getkey/blob/master/tools/keys.txt)