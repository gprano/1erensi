# Projet de dessin d'une rue aléatoire avec turtle

## Objectif

Le but de ce projet est de dessiner une rue aléatoire, qui change à chaque exécution, avec le module turtle. L'intérêt principal pour l'apprentissage de la programmation est que vous allez créer beaucoup de fonctions, qui s'utiliseront entre elles.

Il faut au moins parvenir à générer ce type de dessin :

![exemple 1](exemple1.png)

Mais vous êtes libres d'améliorer de la façon que vous voulez :

![exemple 1](exemple2.png)
![exemple 1](exemple3.png)

## Organisation

Le projet sera par groupes (de ~3) qui sont imposés.

Il sera à rendre pour le 10 novembre (après 2.5 séances en classe) avec une petite présentation à l'oral.

Un lien nextcloud vous sera envoyé pour pouvoir partager votre code dans le groupe et pouvoir continuer chez vous.

## Critères d'évaluation

- Fonctionnement du code
- Clarté et documentation du code : noms de variables et de fonctions bien choisis, docstring claire, utilisation des boucles plutôt que de copié/collé.
- Sérieux du travail dans les séances en classe et implication au sein du groupe.
- Améliorations intéressantes ou originales.

## Étapes

* Dessinez l'arbre de dépendance des fonctions que vous allez créer (par exemple la fonction immeuble utilisera la fonction étage, qui utilisera...). 

* Faites les fonctions depuis les plus simples jusqu'aux plus complexes.

* À la base vous aurez besoin d'une fonction :

    * `rectangle(x, y, largeur, hauteur, couleur)`
    * `trait(x1, y1, x2, y2, couleur, largeur)`


* La plupart des fonctions auront en paramètre les coordonnées `x,y` de l'endroit où dessiner : c'est pratique de toujours considérer que c'est les coordonnées du coin en **bas à gauche** de la forme à dessiner.

* Commencer par faire une version fonctionnelle basique avant de faire les améliorations.

* Testez vos fonctions au fur et à mesure.

* Écrivez une petite docstring qui dit ce que va faire la fonction **avant** d'en écrire le code.

* Faites des dessins au brouillon avec les dimensions pour bien écrire le code.

## Aides

#### Basique

Pour importer ce dont on a besoin :

```python
from turtle import *
from random import randint, random
```

Les fonctions `turtle` dont vous avez besoin sont :

```python
forward(nb_pixels)
backward(nb_pixels)

left(nb_degres)
right(nb_degres)

goto(x,y) # se déplace aux coordonnées x,y

penup() # lève le stylo
pendown() # abaisse le stylo
pensize(largeur) # largeur du trait : 1 au minimum

speed(valeur) # de 1 (lent) à 10 (rapide) et 0 le plus rapide
hideturtle() # cache la tortue
```

Et du module random :

```python
x = randint(a, b) # entier aléatoire entre a et b (inclus)

y = random() # float entre 0 et 1
if y < 0.45: # pour faire quelque chose avec probabilité 45%
    ...
```

#### Gestion des couleurs

On exécutera `colormode(255)` pour avoir des couleurs RVB (rouge vert bleu) dont les valeurs sont entre 0 et 255.

Cette fonction renvoie une couleur aléatoire sous forme d'un triplet (un type qu'on n'a pas encore vu en python, mais vous n'en avez pas besoin ailleurs) :

```python
def couleur_aleatoire():
    r = randint(0,255)
    g = randint(0,255)
    b = randint(0,255)
    return (r,g,b)
```

Vous pouvez ensuite stocker ce triplet dans une variable, et définir la couleur ainsi :

```python
couleur = couleur_aléatoire()
blanc = (255, 255, 255)
noir = (0, 0, 0)
rouge = (255, 0, 0)

color(couleur) # couleur du trait
fillcolor(rouge) # couleur du remplissage
```

#### Remplissage

Il suffit d'appeler `begin_fill()`, de tracer la forme à remplir, puis d'appeler `end_fill()` et turtle remplira la forme avec la couleur de remplissage.

#### Autres

L'aide complète de turtle en français est [ici](https://docs.python.org/fr/3.8/library/turtle.html) si vous avez besoin d'autres fonctions (par exemple `circle` pour tracer un cercle).