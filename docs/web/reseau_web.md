# Web : Rappels (et plus) de SNT pour le HTML/CSS 

### Créer un projet avec VSCodium

Dans le terminal, créez un dossier `rappels_web`, déplacez-vous dedans et ouvrez codium
à cet endroit (avec `mkdir rappels_web`, `cd rappels_web` et `codium .`).

Créez un nouveau fichier `index.html` (icône + à côté du nom de votre dossier dans l'onglet explorateur), et un nouveau fichier `style.css`.

Codium intègre par défaut Emmet, une extension qui permet d'écrire plus rapidement les langages
du web. Dans le fichier html, tapez `!` et prenez la première suggestion (avec la touche tab)
pour obtenir un squelette html de départ.

Installez l'extension **Live Preview** qui permet de voir directement un aperçu de la page
web dans Codium. Vous pouvez ensuite l'ouvrir avec l'icône en haut à droite de l'éditeur.

!!! info
    Pour ne pas avoir à se rappeler d'enregistrer, dans Codium vous pouvez cocher "Fichier > Enregistrement automatique"
    qui enregistre au fur et à mesure toutes les modifications que vous faites.

### HTML

!!! info "Règles du HTML"
    Le langage HTML est un langage de **balises** comme `<body>` ou `<p>`.

    * La plupart fonctionnent par paire avec une balise ouvrante comme `<body>`
    et ensuite une balise fermante `</body>`. Ce qu'il y a entre les deux est le contenu
    de la balise body.
    * Il ne **peut pas** y avoir de chevauchement de balise, par exemple
    `<div> <p> </div> </p>`. Une balise doit être complétement à l'intérieur d'une
    autre ou à côté.
    * Certaines balises sont dites **auto-fermantes** et ne marchent pas par paire,
    parce qu'il n'y a pas lieu de mettre du contenu dans la balise. C'est le cas de
    `link, meta, img, br`.
    * Il peut y avoir des **attributs** dans la balise ouvrante, dont la valeur est donnée
    avec `=` et entre guillemets : par exemple l'attribut `href` de la balise `a` donne
    l'adresse du lien : `<a href="https://tnsi.xyz">Site de NSI</a>`

Comme une balise peut en contenir une autre, on a une **hiérarchie** des balises contenues
les unes dans les autres qu'on peut représenter comme ceci :

![arbre html](dom.png)

On appelle ce type de représentation un **arbre**, qu'on verra plus en détail en Terminale.
On remarque qu'en haut il y a la balise html qui contient toutes les balises de la page.

**Exercice :** Complétez votre fichier `index.html` pour qu'il corresponde à l'arbre de la
figure précédente. Vous pouvez ajouter un peu de texte dans les balises pour pouvoir visualiser
le résultat, ainsi qu'une image rajoutée dans le dossier pour la balise img.


### CSS

Pour qu'une page html utilise un fichier css, il faut une balise `link` qui fasse le lien avec :

* l'attribut `rel` de valeur `stylesheet` pour indiquer que c'est une feuille de style
* l'attribut `href` qui donne l'adresse (relative ou absolue) du fichier css

Ajoutez ce lien vers votre fichier `style.css` dans la page html.

!!! info "Règles du CSS"
    Le CSS est composé d'une séries de règles qui s'écrivent toutes :
    ```css
    sélecteur { 
        propriété : valeur;
        autre-propriété : valeur;
        (etc)
    }
    ```

    * Le sélecteur indique sur quelles balises appliquer le style. 
        * Un nom de balise comme `ul` l'appliquera à toutes les balises `ul` de la page. 
        * On peut cibler la seule balise à laquelle on a mis l'attribut `id="mon_id"` avec le sélecteur `#mon_id` 
        * et toutes les balises qui ont l'attribut `class="ma_classe"` avec le sélecteur `.ma_classe`

**Exercice :** utilisez les propriété `text-align` pour centrer le titre h1, `height` et `width` pour changer la taille de l'image, et `background-color` pour changer la couleur d'arrière-plan de la liste créée avec `ul`.

#### Le "modèle en boîte" de CSS

Pour positionner les éléments, CSS considère qu'il y a deux types de balises :

* les balises **inline** qui sont pour de petits éléments à l'intérieur d'un texte par exemple, et dont on ne peut pas changer la largeur/hauteur avec `width/height`. Ce sont les balises
span, a, img, b...
* les balises **block** qui prennent toute la largeur de la page par défaut et commencent sur une nouvelle ligne. Par exemple p, div, body, html, h1, ...

Ces dernières sont considérées comme des boîtes avec :

* le contenu au milieu
* une bordure éventuelle, avec la propriété CSS `border`
* une marge à l'intérieur de la bordure avec la propriété `padding` et une marge à l'extérieur de la bordure avec la propriété `margin`

![box model](css-box-model.png)

**Exercice :** Pour visualiser les boîtes de votre page html, mettez la valeur `1px solid black`
à la propriété `border` de toutes les balises de la page. Pour cela vous pouvez mettre le sélecteur universel `*` en CSS.

**Exercice :** En utilisant un div à l'intérieur d'un autre, des id pour les différencier, et
ces propriété de `width,height,margin,padding,border,background-color`, reproduisez l'image suivante :

![boite](box.png)

### Serveur et réseau

Dans le terminal, déplacez-vous dans le dossier de votre projet et lancez la commande :

```bash
python3 -m http.server
```

* Que fait cette commande ?

* À quoi sert le numéro de port dans une connexion ?

* Dans un autre terminal, utiliser la commande `ip addr` pour obtenir l'adresse ipv4 de
votre machine, `host NOM_DE_LA_MACHINE` pour obtenir celle d'un autre ordinateur de la salle
avec par exemple `e202-p10` comme nom de machine. Utilisez `ping` pour tester la connexion
avec une autre adresse ip.

* Servez-vous de ces informations pour vous accéder aux pages des autres élèves de la salle qui auront lancé le serveur.

!!! info "page de manuel"
    Pour connaître l'usage et les options d'une commande, on utilise `man NOM_DE_LA_COMMANDE`.
    
    * On peut utiliser `/` pour chercher dans la page (et chercher `-i` par exemple pour savoir ce que fait cette option) avec les raccourcis `n` et `N` pour passer à l'occurence
        suivante ou précédente du motif de recherche. 
    * On peut aussi se déplacer dans la page avec
        les touches PageUp et PageDown ou les flèches haut/bas. 
    * On quitte la page de man avec la touche `q`.

* Cherchez à quoi sert l'option `-t` de la commande `ping`.

* Quelle erreur obtiendra-t-on si on cherche un fichier qui n'existe pas sur le serveur ? C'est une erreur de quel protocole de communication ?


### Exercice : tableau en CSS

Essayez de reproduire en CSS [Ce tableau de Piet Mondrian](https://en.wikipedia.org/wiki/Composition_with_Red_Blue_and_Yellow).

Pour cela :

* Créez un div avec la propriété CSS `position:relative`, une taille et une hauteur fixées
en pixels, pour contenir tout le tableau.
* Créez des div à l'intérieur pour chacun des sous-rectangles avec la propriété `position:absolute` qui permettra de les placer par rapport à tout le tableau sans qu'ils interfèrent entre eux. Changez leur couleur, leur taille et leur hauteur et placez les
à l'aide de `margin`.
